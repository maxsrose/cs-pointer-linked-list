﻿namespace PointerLinkedList.LinkedList;

public interface ILinkedList<T>
{
    void Add(T item);
    void AddAt(T item, int index) { }
    
    T Get(int index);
    
    void Remove(T item);
    void RemoveAt(int index) { }

    int Count();
}