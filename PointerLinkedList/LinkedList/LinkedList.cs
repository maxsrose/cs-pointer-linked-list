﻿using System.Collections;
using System.Runtime.InteropServices;

namespace PointerLinkedList.LinkedList;

public unsafe class LinkedList<T> : ILinkedList<T>
{
    private GCHandle? _handle;
    private ListItem<T>? _list;
    
    public void Add(T item)
    {
        if (_list == null)
        {
            _handle = GCHandle.Alloc(new ListItem<T>(item), GCHandleType.Pinned);
            _list = (ListItem<T>?)_handle?.Target;
            return;
        }

        var lastElement = _list;

        while (lastElement!.HasNext())
            lastElement = _list.Next();
        
        lastElement.Add(item);
    }
    
    public void AddAt(T item, int index)
    {
        if (index < 0 || _list == null)
            throw new Exception();

        if (index == 0)
        {
            var handle = _handle;
            
            _handle = GCHandle.Alloc(new ListItem<T>(item), GCHandleType.Pinned);
            _list = (ListItem<T>?)_handle?.Target;
            
            _list?.Add(handle);
            
            return;
        }

        var i = 0;
        var elem = _list;
        
        while (i < index - 1 && elem.HasNext())
        {
            elem = elem.Next();
            i++;
        }

        var oldNext = elem?.NextHandle();
        elem?.Add(item);
        elem?.Next()?.Add(oldNext);
    }

    public T Get(int index)
    {
        if (index < 0 || _list == null)
            throw new Exception();

        if (index == 0)
            return _list.Get();
        
        var i = 0;
        var item = _list;
        T? output = default; 

        while (_list.HasNext())
        {
            if (i == index)
            {
                output = item.Get();
                break;
            }
            
            item = item?.Next();
            i++;
        }

        if (output == null)
            throw new Exception(); 
        
        return output;
    }

    public void Remove(T item)
    {
        if (_list == null)
            return;
        
        var prevElem = _list;
        var elem = _list;
        var firstItem = true;
        
        while (elem!.HasNext() && !elem.Get()!.Equals(item))
        {
            firstItem = false;
            prevElem = elem;
            elem = _list.Next();
        }
        
        if(firstItem)
        {
            var handle = _handle;
            _handle = elem.NextHandle();
            _list = elem.Next();
            handle?.Free();
        }
        else
        {
            var handle = prevElem.NextHandle();
            prevElem.Add(elem.NextHandle());
            handle?.Free();
        }
    }

    public void RemoveAt(int index)
    {
        if (index < 0 || _list == null)
            throw new Exception();

        if (index == 0)
        {
            var rootHandle = _handle;

            _handle = _list.NextHandle();
            _list = (ListItem<T>?)_handle?.Target;
            
            rootHandle?.Free();
            
            return;
        }

        var i = 0;
        var elem = _list;

        while (_list.HasNext() && i < index - 1)
        {
            elem = elem.Next();
            i++;
        }

        var handle = elem?.NextHandle();
        elem?.Add(elem?.Next()?.NextHandle());
        handle?.Free();
    }

    public int Count()
    {

        if (_list == null)
            return 0;
        
        var count = 1;

        var item = _list;
        while (item.HasNext())
        {
            item = item.Next()!;
            count++;
        }

        return count;
    }

    internal class ListItem<TI> : IDisposable
    {
        private GCHandle _itemHandle;

        private GCHandle? _nextHandle;
        
        internal ListItem(TI item)
        {
            _itemHandle = GCHandle.Alloc(item, GCHandleType.Pinned);
        }

        public TI Get()
        {
            return (TI)_itemHandle.Target!;
        }
        
        public bool HasNext()
        {
            return _nextHandle != null;
        }

        public GCHandle? NextHandle()
        {
            return _nextHandle;
        }
        
        public ListItem<TI>? Next()
        {
            return (ListItem<TI>?)_nextHandle?.Target;
        }

        public void Add(TI next)
        {
            _nextHandle = GCHandle.Alloc(new ListItem<TI>(next), GCHandleType.Pinned);
        }
        
        public void Add(GCHandle? next)
        {
            _nextHandle = next;
        }

        public void Dispose()
        {
            _itemHandle.Free();

            ((ListItem<TI>?)_nextHandle?.Target)?.Dispose();
            _nextHandle?.Free();
        }
    }
}