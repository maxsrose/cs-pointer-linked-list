using PointerLinkedList.LinkedList;

namespace PointerLinkedList.Tests;

public class LinkedListTests
{
    [Fact]
    public void Add_AddsElementToList()
    {
        ILinkedList<int> list = new LinkedList.LinkedList<int>();
        list.Add(42);
        Assert.Equal(1, list.Count());
    }
    
    [Fact]
    public void Add_AddsElementsToList()
    {
        ILinkedList<int> list = new LinkedList.LinkedList<int>();
        list.Add(42);
        list.Add(43);
        Assert.Equal(2, list.Count());
    }

    [Fact]
    public void AddAt_AddsElementAtIndex()
    {
        ILinkedList<int> list = new LinkedList.LinkedList<int>();
        list.Add(10);
        list.Add(20);
        list.AddAt(15, 1);
        Assert.Equal(15, list.Get(1));
    }

    [Fact]
    public void Remove_RemovesElementFromList()
    {
        ILinkedList<string> list = new LinkedList.LinkedList<string>();
        list.Add("apple");
        list.Add("banana");
        list.Remove("apple");
        Assert.Equal("banana", list.Get(0));
    }

    [Fact]
    public void RemoveAt_RemovesElementAtIndex()
    {
        ILinkedList<int> list = new LinkedList.LinkedList<int>();
        list.Add(1);
        list.Add(2);
        list.Add(3);
        list.RemoveAt(1);
        Assert.Equal(3, list.Get(1));
    }

    [Fact]
    public void Get_GetsElementAtIndex()
    {
        ILinkedList<string> list = new LinkedList.LinkedList<string>();
        list.Add("one");
        list.Add("two");
        list.Add("three");
        Assert.Equal("two", list.Get(1));
    }
}